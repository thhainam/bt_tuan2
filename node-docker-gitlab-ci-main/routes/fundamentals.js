const express = require('express');
const router = express.Router();

require('dotenv').config()
const variableData = process.env.variableData || 'fundamentals'

router.get('/', function (req, res, next) {
    res.send({
        name: 'fundamentals',
        server: 'express',
        variableData: variableData
    });
});
//1
router.get('/tinhTong', function (req, res, next) {
    res.send({
        name: 'tinhTong2So',
        sum: sumTwoNumber(1, 2)
    });
});

function sumTwoNumber(a, b) {

    return a + b;
}

//2
router.get('/soSanh', function (req, res, next) {
    res.send({
        result: soSanh(3, 3)
    });
});

function soSanh(a, b) {

    return a === b;
}
//3
router.get('/kiemTraGiaTri', (req, res) => {
    res.send({
        name: 'kiemTraGiaTri',
        result: kiemTraGiaTri("NamHaiTH")
    });
});

function
    kiemTraGiaTri
    (
        a
    ) {

    return typeof a;
}
//4
router.get('/layThuTuChuoi', (req, res) => {
    res.send({
        name: 'layNThuTuChuoi',
        result: layThuTuChuoi("NamHaiTH", 3)
    });
});

function layThuTuChuoi(a, n) {
    return a[n - 1];
}


//5
router.get('/xoaKyTuDauTien', (req, res) => {
    res.send({
        name: 'xoaNKyTuDauTien',
        result: xoaKyTu("NamHaiTH")
    });
});


function
    xoaKyTu
    (
        a
    ) {

    return a.slice(3);
}
//6
router.get('/layKyTuCuoi', (req, res) => {
    res.send({
        name: 'layNKyTuCuoi',
        result: layKyTuCuoi("NamHaiTH")
    });
});


function
    layKyTuCuoi
    (
        str
    ) {

    return str.slice(-3);
}
//7
router.get('/layKyTuDau', (req, res) => {
    res.send({
        name: 'layNKyTuDau',
        result: layKyTuDau("NamHaiTH")
    });
});

function layKyTuDau(a) {
    return a.slice(0, 3);
}

//8
router.get('/timViTri', (req, res) => {
    res.send({
        name: 'timViTriChuoi',
        result: timViTriChuoi("praise the lord")
    });
});


function
    timViTriChuoi
    (
        a
    ) {

    return a.indexOf('is');
}
//9
router.get('/trichXuatDauChuoi', (req, res) => {
    res.send({
        name: 'trichXuatDauChuoi',
        result: trichXuatChuoi("NamHaiTH")
    });
});


function
    trichXuatChuoi
    (
        a
    ) {

    return a.slice(0, a.length / 2);
}
//10
router.get('/xoaKyTuCuoi', (req, res) => {
    res.send({
        name: 'xoaNKyTuCuoi',
        result: xoaKyTuCuoi("NamHaiTH")
    });
});


function
    xoaKyTuCuoi
    (
        a
    ) {

    return a.slice(0, -3);
}
//11
router.get('/traVePhanTram', (req, res) => {
    res.send({
        name: 'traVePhanTramCuaMotSo',
        result: traVePhanTram(100, 20)
    });
});


function
    traVePhanTram
    (
        a, b
    ) {

    return b / 100 * a
}
//12
router.get('/tinhToan6So', (req, res) => {
    res.send({
        name: 'tinhToan6So',
        result: tinhToan6So(6, 5, 4, 3, 2, 1)
    });
});


function
    tinhToan6So
    (
        a, b, c, d, e, f
    ) {

    return (((a + b - c) * d) / e) ** f;
}
//13
router.get('/kiemTraGiaTriChuoi', (req, res) => {
    res.send({
        name: 'kiemTraGiaTriCuaChuoi',
        result: kiemTraGiaTriChuoi('I think', 'therefore I am')
    });
});


function
    kiemTraGiaTriChuoi
    (
        a, b
    ) {

    return a.indexOf(b) === -1 ? a + b : b + a
}
//14
router.get('/kiemTraSoChan', (req, res) => {
    res.send({
        name: 'kiemTraSoChan',
        result: kiemTraSoChan(10)
    });
});


function kiemTraSoChan(a) {
    return a % 2 === 0
}
//15
router.get('/demKyTuXuatHien', (req, res) => {
    res.send({
        name: 'demSoLanKyTuXuatHien',
        result: demKyTuXuatHien('string', 's')
    });
});


function demKyTuXuatHien(a, b) {
    return b.split(a).length - 1
}
//16
router.get('/kiemTraSoNguyen', function (req, res, next) {
    res.send({
        name: 'kiemTraSoNguyen',
        isInteger: kiemTraSoNguyen(1.5)
    });
});

function kiemTraSoNguyen(a) {
    return a === Math.floor(a);
}

//17
router.get('/tinhNhanChiaSoSanh', function (req, res, next) {
    res.send({
        name: 'tinhNhanChiaSoSanh',
        result: tinhNhanChiaSoSanh(5, 2)
    });
});

function tinhNhanChiaSoSanh(a, b) {
    return a < b ? a / b : a * b;
}

//18
router.get('/lamTronSTP', function (req, res, next) {
    res.send({
        name: 'lamTronSoThapPhan',
        result: lamTronSTP(3.14159)
    });
});

function lamTronSTP(a) {
    return Number(a.toFixed(2));
}

//19
router.get('/chiaSoThanhMang', function (req, res, next) {
    res.send({
        name: 'chiaSoThanhMang',
        result: chiaSoThanhMang(12345)
    });
});

function chiaSoThanhMang(a) {
    return Array.from(String(a), Number);
}

module.exports = router;
